# Execução:

* npm start
* npm run android (em outro terminal). Isto irá instalar o app no dispositivo. Após isso basta executá-lo.

# Bibliotecas utilizadas:
* Redux -> Para controlar e organizar melhor o estado da aplicação.
* React Native Navigation -> Controlar a navegação entre as telas.
* Formik -> Criação do formulário de login
* Yup -> Validação dos campos
* React Native Vector Icons - Ícones

### P.S: Eu recebi no e-mail o link para o teste Android, para ser feito em React Native. Portanto não havia tido acesso às informações de que os 3 endpoints da API deveriam ser utilizados, além de algumas outras informações. Mas consegui finalizar o teste :-)
